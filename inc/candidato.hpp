#include <iostream>
#include <string>

using namespace std;

class Candidato {

	private:
		//Atributos
		string nm_candidato; //nome do candidato
		string nm_urna_candidato;//apelido do candidato
		string sg_partido;//partido do candidato
		string ds_genero;//gênero do candidato
		string ds_ocupacao;//ocupação do candidato
		string ds_cargo;//cargo do candidato
		string nr_candidato;//numero do candidato
		int nr_idade;//idade do candidato
	public:
		Candidato();
		Candidato(string nm_ue, string ds_cargo, int nr_candidato, string nm_urna_candidato, string nm_partido);
		~Candidato();
		string get_nm_candidato();
		void set_nm_candidato(string nm_candidato);
		string get_nm_urna_candidato();
		void set_nm_urna_candidato(string nm_urna_candidato);
		string get_sg_partido;
		void set_sg_partido(string sg_partido);
		string get_ds_genero;
		void set_ds_genero(string ds_genero);
		string get_ds_ocupacao;
		void set_ds_ocupacao(string ds_ocupacao);
		string get_ds_cargo;
		void set_ds_cargo(string ds_cargo);
		string get_nr_candidato;
		void set_nr_candidato(string nr_candidato);
		string get_nr_idade;
		void set_nr_idade(int nr_idade);

};
