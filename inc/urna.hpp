
#include <iostream>
#include <vector>
#include <string.h>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <algorithm>
#include <dirent.h>
#include "voto.hpp"
#include "candidato.hpp"

using namespace std;

class Urna {
    private:
      vector<Candidato> * candidatos;
      vector<Voto> votos;
      void criaVoto(int nroCandidato, string tipoCandidato, string estadoEleitor, string nomeEleitor);
      void carregaCandidatos();
      void mostraVencedor();
      int contaVotos(vector<Voto> votos);
      void votaPresidente(string nomeEleitor, string estadoEleitor);
      void votaGovernador(string nomeEleitor, string estadoEleitor);
      void votaSenador(string nomeEleitor, string estadoEleitor);
      void votaDeputadoEstadual(string nomeEleitor, string estadoEleitor);
      void votaDeputadoDistrital(string nomeEleitor, string estadoEleitor);
      void votaDeputadoFederal(string nomeEleitor, string estadoEleitor);
    public:
      Urna();
      bool mostraCandidato(int nroCandidato, string ds_cargo);
      bool mostraCandidato(int nroCandidato, string ds_cargo, string estado);
      void votacao();

};
