#include "candidado.hpp"

Candidato::Candidato(){
	nome = "Nome Sobrenome";
	apelido = "Apelido";
	email= "candidato@email.com";
	partido = "PT";
	uf_nascimento = "UF";
	sexo = "Masculino";
	ocupacao = "Trabalhador";
	cargo = "Cargo";
	num = "00000";
	idade = "00";
}

string Candidato::get_nome(){
	return nome;
}
void Candidato::set_nome(string nome){
	this-> nome = nome;
}
string Candidato::get_apelido(){
	return apelido;
}
void Candidato::set_apelido(string apelido){
	this-> apelido = apelido;
}
string Candidato::get_email(){
	return email;
}
void Candidato::set_email(string email){
	this-> email = email;
}
string Candidato::get_partido(){
	return partido;
}
void Candidato::set_partido(string partido){
	this-> partido = partido;
}
string Candidato::get_uf_nascimento(){
	return uf_nascimento;
}
void Candidato::set_uf_nascimento(string uf_nascimento){
	this-> uf_nascimento = uf_nascimento;
}
string Candidato::get_sexo(){
	return sexo;
}
void Candidato::set_sexo(string sexo){
	this-> sexo = sexo;
}
string Candidato::get_ocupação(){
	return ocupação;
}
void Candidato::set_ocupação(string ocupação){
	this-> ocupação = ocupação;
}
string Candidato::get_cargo(){
	return cargo;
}
void Candidato::set_cargo(string cargo){
	this-> cargo = cargo;
}
string Candidato::get_num{
	return num;
}
void Candidato::set_num(string num){
	this-> num = num;
}
int Candidadto::get_idade(){
	return idade;
}
void Candidato::set_idade(int idade){
	this-> idade = idade;
}


