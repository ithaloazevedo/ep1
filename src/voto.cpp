#include "voto.hpp"

Voto::Voto(string eleitor, string tipoVoto, string estadoEleitor, int nroCandidato){
    this->eleitor = eleitor;
    this->tipoVoto = tipoVoto;
    this->nroCandidato = nroCandidato;
    this->estadoEleitor = estadoEleitor;
}
void Voto::setEleitor(string eleitor){
    this->eleitor = eleitor;
}
string Voto::getEleitor(){
    return eleitor;
}
void Voto::setTipoVoto(string tipoVoto){
    this->tipoVoto = tipoVoto;
}
string Voto::getTipoVoto(){
    return tipoVoto;
}
void Voto::setNroCandidato(int nroCandidato){
    this->nroCandidato = nroCandidato;
}
int Voto::getNroCandidato(){
    return nroCandidato;
}
string Voto::getEstadoEleitor(){
    return estadoEleitor;
}
void Voto::setEstadoEleitor(int estadoEleitor){
    this->estadoEleitor = estadoEleitor;
}
